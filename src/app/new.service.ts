import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class NewService {
  constructor(private readonly http: HttpClient) {}

  getNews(): Observable<any> {
    return this.http.get<any>(
      'http://dev-sw6-uapi.ecm.in.th/uapi/drt-ElectronicsDocument/ED-GetNews?EmployeeId=3'
    );
  }

  updateStatus(
    newsId: number,
    status: number,
    employeeId: number = 3
  ): Observable<any> {
    return this.http.post<any>(
      'http://dev-sw6-uapi.ecm.in.th/uapi/drt-ElectronicsDocument/ED-UpdateFormBook',
      {
        EmployeeId: employeeId,
        NewsId: newsId,
        Status: status,
      },
      {
        headers: new HttpHeaders().set('Content-Type', 'form-data')
      }
    );
  }
}
