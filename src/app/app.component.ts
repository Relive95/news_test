import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { NewService } from './new.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  $subscriptions!: Subscription;
  display: boolean = false;
  newList: any = [];
  dataViewState: any = null
  

  constructor(private newService: NewService) {}

  ngOnInit(): void {
    this.newService.getNews().subscribe((result) => {
      this.newList = result.data;
    });
  }

  showDialog(item:any) {
    this.display = true;
    this.dataViewState = item;
  }

  onChangeStatus(event: any, newsId: number): void {
    const status: number = event.checked ? 1 : 0;
    this.newService.updateStatus(newsId, status).subscribe((result) => {
      if (result.successful) {
        const index: any = this.newList.findIndex(
          (news: any) => news.NewsId === newsId
        );
        this.newList[index].Status = event.checked ? 1 : 0;
      }
    });
  }

  ngOnDestroy(): void {
    this.$subscriptions?.unsubscribe();
  }
}
